﻿using System;                   // -|
using System.Drawing;           //  | Простори імен для роботи форми
using System.Windows.Forms;     // -|
using CarImitation.Properties;  // Простір імен для ресурсів
using Transitions;              // Простір імен для роботи з бібліотекою Transition(анімація)

namespace CarImitation          // Простір імен цієї програми
{
    public partial class MainForm : Form    //Основний клас програми – наслідується від класу Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) //Дія при натисненні кнопки "Імітувати"
        {
            #region Перевірка правильності заповнення форми
            var flag = true;     //flag=true - форма заповнена, інакше – помилки

            int tmp1;            //Сюди поміміститься дані з поля 1, якщо це можливо
            if (!int.TryParse(tX1.Text, out tmp1) || (tmp1 > 120) || (tmp1 < 12)) 
                //перевірка чи в полі X1 - число, і чи воно в межах (12;120)
                flag = false; //якщо ні – помилка заповнення форми

            float tmp4;
            if (!float.TryParse(tX4.Text, out tmp4) || (tmp4 > 5) || (tmp4 < 0))
                //перевірка чи в полі X4 - число(десяткове), і чи воно в межах (0.0;5.0)
                flag = false; //якщо ні – помилка заповнення форми

            if (!flag) //Якщо присутні помилки в заповненні
            {
                //Виводимо повідомленя
                MessageBox.Show("Перевірте правильність введених даних!", "Помилка введення даних!", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                //виходимо з функції. (на форму вводу параметрів)
                return;
            }
            #endregion
            
            //Якщо ж форма нормально заповненна
            //Надає фокус машині (це не обов’язково - але вирішує баг машиною що їздить поверх вводу параметрів)
            pCar.Focus();
            lInfo.Visible = false;  //ховаємо інформацію про те чи відбулась аварія чи ні
            pBang.Visible = false;  //ховаємо картинку вибуху
            pCar.Location = new Point(12,201); //координати початкового розміщення першої машини
            pCar_orange.Location = new Point(780,141); //координати розміщення другої машини

            #region Занесення параметрів
            var paramsArray = new int[5]; //Масив параметрів 
            //Записуємо в 0 елемент масиву значення параметру з поля 1: 0 якщо воно (20;65) і 1 в іншому випадку
            paramsArray[0] = (tmp1 >= 20 && tmp1 <= 65) ? 0 : 1;
            //Записуємо в 1 елемент масиву значення параметру з поля 2:
            paramsArray[1] = tX2.SelectedIndex == 1 ? 1 : 0;
            //Записуємо в 2 елемент масиву значення параметру з поля 3:
            paramsArray[2] = double.Parse(tX4.Text) >= 0.03 ? 1 : 0;
            //Записуємо в 3 елемент масиву значення параметру з поля 4:
            paramsArray[3] = tX6.SelectedIndex == 0 ? 0 : 1;
            //Записуємо в40 елемент масиву значення параметру з поля 5:
            paramsArray[4] = tX7.SelectedIndex == 0 ? 0 : 1;
            #endregion
            panel2_Click(this, new EventArgs()); //Виконання функція,що плавно ховає форму вводу значень

            var p = CalculateProbability(paramsArray);//Обчислення ймовірності за формулою

            if (p > 0.5) //Якщо ймовірність більша за 0,5 тоді
            {
                //Заносимо текст, що аварія відбудеться та ймовірність (ще не виводимо сам текст)
                lInfo.Text = String.Format("АВАРІЯ ВІДБУДЕТЬСЯ!\nЙМОВІРНІСТЬ {0:0.0}%", p * 100);
                lInfo.ForeColor = Color.OrangeRed; //червоним кольором
                DrawAccident();  //Виконуємо функцію імітації руху автомобілів при аварії
            }
            else
            {
                //Заносимо текст, що аварія відбудеться та ймовірність (ще не виводимо сам текст)
                lInfo.Text = String.Format("АВАРІЯ НЕ ВІДБУДЕТЬСЯ!\nЙМОВІРНІСТЬ {0:0.0}%",p * 100);
                lInfo.ForeColor = Color.LimeGreen; //зеленим кольором
                DrawNoAccident(); //Виконуємо функцію імітації руху автомобілів при відсутності аварії
            }
            //Показуємо текст, що аварія відбудеться чи ні і ймовірність. :)
            lInfo.Visible = true;
        }

        private void DrawAccident() //Функція "малює" аварію
        {
            //Створюється переміщення 1 - тип прискорення (за 1 секунду)
            var t1 = new Transition(new TransitionType_Acceleration(1000));
            //При якому міняється координата Y синьої машини до 200
            t1.add(pCar, "Left", 200);

            //Створюється переміщення 2 – тип уповільнення (за 1 секунду)
            var t2 = new Transition(new TransitionType_Deceleration(1000));
            t2.add(pCar, "Left", 450); //координата Y - 450
            t2.add(pCar, "Top", 154);  //координата X - 154

            //Виконання переміщень 1 та 2 по черзі (ланцюгом)
            Transition.runChain(t1, t2);

            //Виконання переміщення жовтої машини по координаті Y в точну 600 за 2 сек
            Transition.run(pCar_orange, "Left", 600, new TransitionType_Acceleration(2000));

        }

        public void DrawNoAccident() //Функція "малює" відсутність аварії
        {
            //Рух першої машини
            Transition.run(pCar, "Left", 800, new TransitionType_Acceleration(2000));

            //Рух другої машини
            var t1 = new Transition(new TransitionType_Acceleration(2000));
            t1.add(pCar_orange, "Left", -150);
            Transition.runChain(t1);
        }

        // Функія для обрахунку за формулою (параметри функції – масив з 1 та 0)
        public static double CalculateProbability(int[] parms)
        {
           //Функція рахує за формулою
            var tmp =
                Math.Exp(-2.436 + 1.515*parms[0] + 1.631*parms[1] + 1.921*parms[2] + 1.870*parms[3] + 1.489*parms[4]);
            return tmp / (1 + tmp); //і повертає
        }

        private void MainForm_Load(object sender, EventArgs e)//Дії при завантаженні програми
        {
            //Встановлення значень випадаючих спискув за замовчення 
            //(кругом - перший елемент)
            tX2.SelectedIndex = 0;
            tX6.SelectedIndex = 0;
            tX7.SelectedIndex = 0;
        }

        //Функція виконується коли мишка над панелькою що відсуває форму
        private void panel2_MouseHover(object sender, EventArgs e)
        {
            pSlider.BackColor = Color.FromArgb(35,35,35); //Змінюємо колір на RGB(35,35,35) - темніший ніж оригінал
        }

        //Функція виконується коли мишка не над панелькою що відсуває форму
        private void panel2_MouseLeave(object sender, EventArgs e)
        {
            pSlider.BackColor = Color.FromArgb(45, 45, 45); //Змінюємо колір на RGB(45,45,45) - такий як колір форми
        }

        //Функція анімації анкети (ховання і з’являння)
        private void panel2_Click(object sender, EventArgs e) 
        {
            if (pSlider.Tag == "1") //Якщо анкета видима
                //Ховаємо –– періміщуємо форму в координати Y=-753, типом аніміції уповільнення, за півсекенди
                Transition.run(pForm, "Left", -753, new TransitionType_Deceleration(500));
            else //якщо невидима
                //Показуємо –– періміщуємо форму в координати Y=2, типом аніміції "посте переміщення", за півсекенди
                Transition.run(pForm, "Left", 2, new TransitionType_EaseInEaseOut(500));

            pSlider.Tag = pSlider.Tag == "1" ? "0" : "1"; //Міняємо тег видимості на протилежний
            //Міняємо зображення стілочики на протилежне
            pSlider.BackgroundImage = (pSlider.Tag == "1") ? Resources.slide_down : Resources.slide_up;
        }

        //Функція виконується коли рухається синій автомобіль(синій)
        private void pCar_Move(object sender, EventArgs e) 
        {
            if (pCar.Location.Y > 154) return; //Якщо координата Y <=154 
            pBang.Visible = true;              //То відображається вибух

        }

    }
}
