﻿using System.Windows.Forms;

namespace CarImitation
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tX6 = new System.Windows.Forms.ComboBox();
            this.tX2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lX7 = new System.Windows.Forms.Label();
            this.lX4 = new System.Windows.Forms.Label();
            this.tX4 = new System.Windows.Forms.TextBox();
            this.lX2 = new System.Windows.Forms.Label();
            this.lX1 = new System.Windows.Forms.Label();
            this.tX1 = new System.Windows.Forms.TextBox();
            this.tX7 = new System.Windows.Forms.ComboBox();
            this.pForm = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pSlider = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pCar_orange = new System.Windows.Forms.PictureBox();
            this.pCar = new System.Windows.Forms.PictureBox();
            this.pBang = new System.Windows.Forms.PictureBox();
            this.lInfo = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.pForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCar_orange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBang)).BeginInit();
            this.SuspendLayout();
            // 
            // tX6
            // 
            this.tX6.BackColor = System.Drawing.Color.Black;
            this.tX6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tX6.ForeColor = System.Drawing.Color.Gold;
            this.tX6.FormattingEnabled = true;
            this.tX6.ItemHeight = 14;
            this.tX6.Items.AddRange(new object[] {
            "Сприятливі",
            "Несприятливі"});
            this.tX6.Location = new System.Drawing.Point(297, 186);
            this.tX6.Name = "tX6";
            this.tX6.Size = new System.Drawing.Size(138, 22);
            this.tX6.TabIndex = 14;
            // 
            // tX2
            // 
            this.tX2.BackColor = System.Drawing.Color.Black;
            this.tX2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tX2.ForeColor = System.Drawing.Color.Gold;
            this.tX2.FormattingEnabled = true;
            this.tX2.ItemHeight = 14;
            this.tX2.Items.AddRange(new object[] {
            "Відсутні",
            "Короткозорість",
            "Далекозорість"});
            this.tX2.Location = new System.Drawing.Point(297, 132);
            this.tX2.Name = "tX2";
            this.tX2.Size = new System.Drawing.Size(138, 22);
            this.tX2.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Location = new System.Drawing.Point(144, 285);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 28);
            this.button1.TabIndex = 12;
            this.button1.Text = "Імітувати";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(88, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Погодні умови";
            // 
            // lX7
            // 
            this.lX7.AutoSize = true;
            this.lX7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lX7.Location = new System.Drawing.Point(88, 216);
            this.lX7.Name = "lX7";
            this.lX7.Size = new System.Drawing.Size(76, 14);
            this.lX7.TabIndex = 7;
            this.lX7.Text = "Пора доби";
            // 
            // lX4
            // 
            this.lX4.AutoSize = true;
            this.lX4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lX4.Location = new System.Drawing.Point(88, 162);
            this.lX4.Name = "lX4";
            this.lX4.Size = new System.Drawing.Size(162, 14);
            this.lX4.TabIndex = 5;
            this.lX4.Text = "Рівень алкоголю в крові";
            // 
            // tX4
            // 
            this.tX4.Location = new System.Drawing.Point(297, 159);
            this.tX4.MaxLength = 5;
            this.tX4.Name = "tX4";
            this.tX4.Size = new System.Drawing.Size(138, 22);
            this.tX4.TabIndex = 4;
            this.tX4.Text = "0,03";
            this.tX4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip2.SetToolTip(this.tX4, "Перевірте правильність вказаного значення!");
            this.toolTip1.SetToolTip(this.tX4, "Значення має бути в межах від 0 до 5.\r\nДесяткову частину відділюйте комою.");
            // 
            // lX2
            // 
            this.lX2.AutoSize = true;
            this.lX2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lX2.Location = new System.Drawing.Point(88, 135);
            this.lX2.Name = "lX2";
            this.lX2.Size = new System.Drawing.Size(126, 14);
            this.lX2.TabIndex = 3;
            this.lX2.Text = "Проблеми із зором";
            // 
            // lX1
            // 
            this.lX1.AutoSize = true;
            this.lX1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lX1.Location = new System.Drawing.Point(88, 108);
            this.lX1.Name = "lX1";
            this.lX1.Size = new System.Drawing.Size(62, 14);
            this.lX1.TabIndex = 1;
            this.lX1.Text = "Вік водія";
            // 
            // tX1
            // 
            this.tX1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tX1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tX1.Location = new System.Drawing.Point(297, 105);
            this.tX1.MaxLength = 3;
            this.tX1.Name = "tX1";
            this.tX1.ShortcutsEnabled = false;
            this.tX1.Size = new System.Drawing.Size(138, 22);
            this.tX1.TabIndex = 0;
            this.tX1.Text = "20";
            this.tX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip2.SetToolTip(this.tX1, "Перевірте правильність вказано значння");
            this.toolTip1.SetToolTip(this.tX1, "Значення може бути в межах від 12 до 120");
            // 
            // tX7
            // 
            this.tX7.BackColor = System.Drawing.Color.Black;
            this.tX7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tX7.ForeColor = System.Drawing.Color.Gold;
            this.tX7.FormattingEnabled = true;
            this.tX7.ItemHeight = 14;
            this.tX7.Items.AddRange(new object[] {
            "Світла",
            "Темна"});
            this.tX7.Location = new System.Drawing.Point(297, 213);
            this.tX7.Name = "tX7";
            this.tX7.Size = new System.Drawing.Size(138, 22);
            this.tX7.TabIndex = 15;
            // 
            // pForm
            // 
            this.pForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.pForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.pForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pForm.Controls.Add(this.pictureBox1);
            this.pForm.Controls.Add(this.pSlider);
            this.pForm.Controls.Add(this.label1);
            this.pForm.Controls.Add(this.tX7);
            this.pForm.Controls.Add(this.button1);
            this.pForm.Controls.Add(this.tX6);
            this.pForm.Controls.Add(this.tX1);
            this.pForm.Controls.Add(this.tX2);
            this.pForm.Controls.Add(this.lX1);
            this.pForm.Controls.Add(this.lX2);
            this.pForm.Controls.Add(this.label2);
            this.pForm.Controls.Add(this.tX4);
            this.pForm.Controls.Add(this.lX7);
            this.pForm.Controls.Add(this.lX4);
            this.pForm.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pForm.ForeColor = System.Drawing.Color.Gold;
            this.pForm.Location = new System.Drawing.Point(2, 2);
            this.pForm.Name = "pForm";
            this.pForm.Size = new System.Drawing.Size(762, 408);
            this.pForm.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CarImitation.Properties.Resources.bootprints;
            this.pictureBox1.Location = new System.Drawing.Point(491, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 407);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // pSlider
            // 
            this.pSlider.BackgroundImage = global::CarImitation.Properties.Resources.slide_down;
            this.pSlider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pSlider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pSlider.Location = new System.Drawing.Point(752, 0);
            this.pSlider.Name = "pSlider";
            this.pSlider.Size = new System.Drawing.Size(10, 409);
            this.pSlider.TabIndex = 18;
            this.pSlider.Tag = "1";
            this.pSlider.Click += new System.EventHandler(this.panel2_Click);
            this.pSlider.MouseLeave += new System.EventHandler(this.panel2_MouseLeave);
            this.pSlider.MouseHover += new System.EventHandler(this.panel2_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(198, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 29);
            this.label1.TabIndex = 16;
            this.label1.Text = "ПАРАМЕТРИ";
            // 
            // pCar_orange
            // 
            this.pCar_orange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.pCar_orange.Image = global::CarImitation.Properties.Resources.car2;
            this.pCar_orange.Location = new System.Drawing.Point(780, 141);
            this.pCar_orange.Name = "pCar_orange";
            this.pCar_orange.Size = new System.Drawing.Size(131, 69);
            this.pCar_orange.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pCar_orange.TabIndex = 4;
            this.pCar_orange.TabStop = false;
            // 
            // pCar
            // 
            this.pCar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.pCar.Image = ((System.Drawing.Image)(resources.GetObject("pCar.Image")));
            this.pCar.Location = new System.Drawing.Point(44, 201);
            this.pCar.Name = "pCar";
            this.pCar.Size = new System.Drawing.Size(141, 53);
            this.pCar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pCar.TabIndex = 2;
            this.pCar.TabStop = false;
            this.pCar.Move += new System.EventHandler(this.pCar_Move);
            // 
            // pBang
            // 
            this.pBang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pBang.Image = global::CarImitation.Properties.Resources.dddd;
            this.pBang.Location = new System.Drawing.Point(263, 50);
            this.pBang.Name = "pBang";
            this.pBang.Size = new System.Drawing.Size(502, 260);
            this.pBang.TabIndex = 5;
            this.pBang.TabStop = false;
            this.pBang.Visible = false;
            // 
            // lInfo
            // 
            this.lInfo.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lInfo.Location = new System.Drawing.Point(2, 22);
            this.lInfo.Name = "lInfo";
            this.lInfo.Size = new System.Drawing.Size(763, 73);
            this.lInfo.TabIndex = 6;
            this.lInfo.Text = "АВАРІЯ";
            this.lInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lInfo.Visible = false;
            // 
            // toolTip1
            // 
            this.toolTip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.toolTip1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.toolTip1.ToolTipTitle = "Увага!";
            this.toolTip1.UseFading = false;
            // 
            // toolTip2
            // 
            this.toolTip2.Active = false;
            this.toolTip2.IsBalloon = true;
            this.toolTip2.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.toolTip2.ToolTipTitle = "Помилка!";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.BackgroundImage = global::CarImitation.Properties.Resources.road2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(766, 414);
            this.Controls.Add(this.pForm);
            this.Controls.Add(this.lInfo);
            this.Controls.Add(this.pBang);
            this.Controls.Add(this.pCar);
            this.Controls.Add(this.pCar_orange);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Програма імітації ДТП залежно від параметрів v1.0";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.pForm.ResumeLayout(false);
            this.pForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCar_orange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tX1;
        private System.Windows.Forms.Label lX1;
        private System.Windows.Forms.Label lX2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lX7;
        private System.Windows.Forms.Label lX4;
        private System.Windows.Forms.TextBox tX4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pCar;
        private System.Windows.Forms.ComboBox tX2;
        private System.Windows.Forms.ComboBox tX6;
        private System.Windows.Forms.ComboBox tX7;
        private System.Windows.Forms.Panel pForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pSlider;
        //private System.Windows.Forms.PictureBox pSlideImage;
        private PictureBox pCar_orange;
        private PictureBox pBang;
        private PictureBox pictureBox1;
        private Label lInfo;
        private ToolTip toolTip1;
        private ToolTip toolTip2;
    }
}

